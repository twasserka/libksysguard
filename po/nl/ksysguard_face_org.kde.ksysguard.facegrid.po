# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-01-29 00:45+0000\n"
"PO-Revision-Date: 2021-08-27 15:06+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.0\n"

#: contents/ui/Config.qml:35
#, kde-format
msgid "Number of Columns:"
msgstr "Aantal kolommen:"

#: contents/ui/Config.qml:42
#, kde-format
msgctxt "@label"
msgid "Automatic"
msgstr "Automatisch"

#: contents/ui/Config.qml:54
#, kde-format
msgid "Display Style:"
msgstr "Weergavestijl:"
