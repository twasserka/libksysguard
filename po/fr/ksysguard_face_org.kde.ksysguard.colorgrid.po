# Xavier Besnard <xavier.besnard@neuf.fr>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-13 00:48+0000\n"
"PO-Revision-Date: 2022-10-14 10:40+0200\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.08.1\n"

#: contents/ui/Config.qml:25
#, kde-format
msgid "Use sensor color:"
msgstr "Utiliser la couleur du senseur :"

#: contents/ui/Config.qml:30
#, kde-format
msgid "Number of Columns:"
msgstr "Nombre de colonnes :"

#: contents/ui/Config.qml:37
#, kde-format
msgctxt "@label"
msgid "Automatic"
msgstr "Automatique"
