# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: ksgrd\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-03-01 00:45+0000\n"
"PO-Revision-Date: 2020-07-03 12:07+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-IgnoreConsistency: Traps\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Node md RAID Exaustões min Int FIFO Zombie mW mV mAh\n"
"X-POFile-SpellExtra: mA mWh ACPI nodes Nwid Rx Tx Motherboard PECI\n"
"X-POFile-IgnoreConsistency: Table\n"

#: SensorAgent.cpp:87
#, kde-format
msgctxt "%1 is a host name"
msgid ""
"Message from %1:\n"
"%2"
msgstr ""
"Mensagem de %1:\n"
"%2"

#: SensorManager.cpp:49
#, kde-format
msgid "Change"
msgstr "Modificar"

#: SensorManager.cpp:50
#, kde-format
msgid "Rate"
msgstr "Taxa"

#: SensorManager.cpp:52
#, kde-format
msgid "CPU Load"
msgstr "Carga do CPU"

#: SensorManager.cpp:53
#, kde-format
msgid "Idling"
msgstr "Inactivo"

#: SensorManager.cpp:54
#, kde-format
msgid "Nice Load"
msgstr "Carga de Prioridade"

#: SensorManager.cpp:55
#, kde-format
msgid "User Load"
msgstr "Carga do Utilizador"

#: SensorManager.cpp:56
#, kde-format
msgctxt "@item sensor description"
msgid "System Load"
msgstr "Carga do Sistema"

#: SensorManager.cpp:57
#, kde-format
msgid "Waiting"
msgstr "Em espera"

#: SensorManager.cpp:58
#, kde-format
msgid "Interrupt Load"
msgstr "Carga de Interrupções"

#: SensorManager.cpp:59
#, kde-format
msgid "Total Load"
msgstr "Carga Total"

#: SensorManager.cpp:61
#, kde-format
msgid "Memory"
msgstr "Memória"

#: SensorManager.cpp:62
#, kde-format
msgid "Physical Memory"
msgstr "Memória Física"

#: SensorManager.cpp:63
#, kde-format
msgid "Total Memory"
msgstr "Memória Total"

#: SensorManager.cpp:64
#, kde-format
msgid "Swap Memory"
msgstr "Memória Virtual"

#: SensorManager.cpp:65
#, kde-format
msgid "Cached Memory"
msgstr "Memória em 'Cache'"

#: SensorManager.cpp:66
#, kde-format
msgid "Buffered Memory"
msgstr "Memória de Tampão"

#: SensorManager.cpp:67
#, kde-format
msgid "Used Memory"
msgstr "Memória Utilizada"

#: SensorManager.cpp:68
#, kde-format
msgid "Application Memory"
msgstr "Memória da Aplicação"

#: SensorManager.cpp:69
#, kde-format
msgid "Allocated Memory"
msgstr "Memória Alocada"

#: SensorManager.cpp:70
#, kde-format
msgid "Free Memory"
msgstr "Memória Livre"

#: SensorManager.cpp:71
#, kde-format
msgid "Available Memory"
msgstr "Memória Disponível"

#: SensorManager.cpp:72
#, kde-format
msgid "Active Memory"
msgstr "Memória Activa"

#: SensorManager.cpp:73
#, kde-format
msgid "Inactive Memory"
msgstr "Memória Inactiva"

#: SensorManager.cpp:74
#, kde-format
msgid "Wired Memory"
msgstr "Memória Capturada"

#: SensorManager.cpp:75
#, kde-format
msgid "Exec Pages"
msgstr "Páginas Executáveis"

#: SensorManager.cpp:76
#, kde-format
msgid "File Pages"
msgstr "Páginas de Ficheiros"

#: SensorManager.cpp:79
#, kde-format
msgid "Processes"
msgstr "Processos"

#: SensorManager.cpp:80 SensorManager.cpp:257
#, kde-format
msgid "Process Controller"
msgstr "Controlador de Processos"

#: SensorManager.cpp:81
#, kde-format
msgid "Last Process ID"
msgstr "ID do Último Processo"

#: SensorManager.cpp:82
#, kde-format
msgid "Process Spawn Count"
msgstr "Nº Activações do Processo"

#: SensorManager.cpp:83
#, kde-format
msgid "Process Count"
msgstr "Número de Processos"

#: SensorManager.cpp:84
#, kde-format
msgid "Idle Processes Count"
msgstr "Número de Processos Inactivos"

#: SensorManager.cpp:85
#, kde-format
msgid "Running Processes Count"
msgstr "Número de Processos em Execução"

#: SensorManager.cpp:86
#, kde-format
msgid "Sleeping Processes Count"
msgstr "Número de Processos Suspensos"

#: SensorManager.cpp:87
#, kde-format
msgid "Stopped Processes Count"
msgstr "Número de Processos Parados"

#: SensorManager.cpp:88
#, kde-format
msgid "Zombie Processes Count"
msgstr "Número de Processos 'Zombie'"

#: SensorManager.cpp:89
#, kde-format
msgid "Waiting Processes Count"
msgstr "Número de Processos em Espera"

#: SensorManager.cpp:90
#, kde-format
msgid "Locked Processes Count"
msgstr "Número de Processos Bloqueados"

#: SensorManager.cpp:92
#, kde-format
msgid "Disk Throughput"
msgstr "Rendimento do Disco"

#: SensorManager.cpp:93
#, kde-format
msgctxt "CPU Load"
msgid "Load"
msgstr "Carga"

#: SensorManager.cpp:94
#, kde-format
msgid "Total Accesses"
msgstr "Acessos Totais"

#: SensorManager.cpp:95
#, kde-format
msgid "Read Accesses"
msgstr "Acessos de Leitura"

#: SensorManager.cpp:96
#, kde-format
msgid "Write Accesses"
msgstr "Acessos de Escrita"

#: SensorManager.cpp:97
#, kde-format
msgid "Read Data"
msgstr "Dados Lidos"

#: SensorManager.cpp:98
#, kde-format
msgid "Written Data"
msgstr "Dados Gravados"

#: SensorManager.cpp:99
#, kde-format
msgid "Milliseconds spent reading"
msgstr "Milisegundos gastos em leituras"

#: SensorManager.cpp:100
#, kde-format
msgid "Milliseconds spent writing"
msgstr "Milisegundos gastos em escritas"

#: SensorManager.cpp:101
#, kde-format
msgid "I/Os currently in progress"
msgstr "E/S's actualmente em curso"

#: SensorManager.cpp:102
#, kde-format
msgid "Pages In"
msgstr "Páginas Recebidas"

#: SensorManager.cpp:103
#, kde-format
msgid "Pages Out"
msgstr "Páginas Enviadas"

#: SensorManager.cpp:104
#, kde-format
msgid "Context Switches"
msgstr "Mudanças de Contexto"

#: SensorManager.cpp:105
#, kde-format
msgid "Traps"
msgstr "Rotinas de Interrupção"

#: SensorManager.cpp:106
#, kde-format
msgid "System Calls"
msgstr "Chamadas de Sistema"

#: SensorManager.cpp:107
#, kde-format
msgid "Network"
msgstr "Rede"

#: SensorManager.cpp:108
#, kde-format
msgid "Interfaces"
msgstr "Interfaces"

#: SensorManager.cpp:109
#, kde-format
msgid "Receiver"
msgstr "Destinatário"

#: SensorManager.cpp:110
#, kde-format
msgid "Transmitter"
msgstr "Transmissor"

#: SensorManager.cpp:112
#, kde-format
msgid "Data Rate"
msgstr "Taxa de Dados"

#: SensorManager.cpp:113
#, kde-format
msgid "Compressed Packets Rate"
msgstr "Taxa de Pacotes Comprimidos"

#: SensorManager.cpp:114
#, kde-format
msgid "Dropped Packets Rate"
msgstr "Taxa de Pacotes Perdidos"

#: SensorManager.cpp:115
#, kde-format
msgid "Error Rate"
msgstr "Taxa de Erros"

#: SensorManager.cpp:116
#, kde-format
msgid "FIFO Overruns Rate"
msgstr "Taxa de Exaustões da FIFO"

#: SensorManager.cpp:117
#, kde-format
msgid "Frame Error Rate"
msgstr "Taxa de Erros de Tramas"

#: SensorManager.cpp:118
#, kde-format
msgid "Multicast Packet Rate"
msgstr "Taxa de Pacotes Difundidos"

#: SensorManager.cpp:119
#, kde-format
msgid "Packet Rate"
msgstr "Taxa de Pacotes"

#: SensorManager.cpp:120
#, kde-format
msgctxt "@item sensor description ('carrier' is a type of network signal)"
msgid "Carrier Loss Rate"
msgstr "Taxa de Perdas da Portadora"

#: SensorManager.cpp:121 SensorManager.cpp:132
#, kde-format
msgid "Collisions"
msgstr "Colisões"

#: SensorManager.cpp:123
#, kde-format
msgid "Data"
msgstr "Dados"

#: SensorManager.cpp:124
#, kde-format
msgid "Compressed Packets"
msgstr "Pacotes Comprimidos"

#: SensorManager.cpp:125
#, kde-format
msgid "Dropped Packets"
msgstr "Pacotes Perdidos"

#: SensorManager.cpp:126
#, kde-format
msgid "Errors"
msgstr "Erros"

#: SensorManager.cpp:127
#, kde-format
msgid "FIFO Overruns"
msgstr "Exaustões da FIFO"

#: SensorManager.cpp:128
#, kde-format
msgid "Frame Errors"
msgstr "Erros de Tramas"

#: SensorManager.cpp:129
#, kde-format
msgid "Multicast Packets"
msgstr "Pacotes em Difusão"

#: SensorManager.cpp:130
#, kde-format
msgid "Packets"
msgstr "Pacotes"

#: SensorManager.cpp:131
#, kde-format
msgctxt "@item sensor description ('carrier' is a type of network signal)"
msgid "Carrier Losses"
msgstr "Perdas da Portadora"

#: SensorManager.cpp:135
#, kde-format
msgid "Sockets"
msgstr "'Sockets'"

#: SensorManager.cpp:136
#, kde-format
msgid "Total Number"
msgstr "Número Total"

#: SensorManager.cpp:137 SensorManager.cpp:258
#, kde-format
msgid "Table"
msgstr "Tabela"

#: SensorManager.cpp:138
#, kde-format
msgid "Advanced Power Management"
msgstr "Gestão Avançada da Energia"

#: SensorManager.cpp:139
#, kde-format
msgid "ACPI"
msgstr "ACPI"

#: SensorManager.cpp:140
#, kde-format
msgid "Cooling Device"
msgstr "Dispositivo de Arrefecimento"

#: SensorManager.cpp:141
#, kde-format
msgid "Current State"
msgstr "Estado Actual"

#: SensorManager.cpp:142 SensorManager.cpp:143
#, kde-format
msgid "Thermal Zone"
msgstr "Zona Térmica"

#: SensorManager.cpp:144 SensorManager.cpp:145
#, kde-format
msgid "Temperature"
msgstr "Temperatura"

#: SensorManager.cpp:146
#, kde-format
msgid "Average CPU Temperature"
msgstr "Temperatura Média do CPU"

#: SensorManager.cpp:147
#, kde-format
msgid "Fan"
msgstr "Ventoinha"

#: SensorManager.cpp:148
#, kde-format
msgid "State"
msgstr "Estado"

#: SensorManager.cpp:149
#, kde-format
msgid "Battery"
msgstr "Bateria"

#: SensorManager.cpp:150
#, kde-format
msgid "Battery Capacity"
msgstr "Capacidade da Bateria"

#: SensorManager.cpp:151
#, kde-format
msgid "Battery Charge"
msgstr "Carga da Bateria"

#: SensorManager.cpp:152
#, kde-format
msgid "Battery Usage"
msgstr "Utilização da Bateria"

#: SensorManager.cpp:153
#, kde-format
msgid "Battery Voltage"
msgstr "Tensão da Bateria"

#: SensorManager.cpp:154
#, kde-format
msgid "Battery Discharge Rate"
msgstr "Taxa de Descarga da Bateria"

#: SensorManager.cpp:155
#, kde-format
msgid "Remaining Time"
msgstr "Tempo Restante"

#: SensorManager.cpp:156
#, kde-format
msgid "Interrupts"
msgstr "Interrupções"

#: SensorManager.cpp:157
#, kde-format
msgid "Load Average (1 min)"
msgstr "Carga Média (1 min)"

#: SensorManager.cpp:158
#, kde-format
msgid "Load Average (5 min)"
msgstr "Carga Média (5 min)"

#: SensorManager.cpp:159
#, kde-format
msgid "Load Average (15 min)"
msgstr "Carga Média (15 min)"

#: SensorManager.cpp:160
#, kde-format
msgid "Clock Frequency"
msgstr "Frequência do Relógio"

#: SensorManager.cpp:161
#, kde-format
msgid "Average Clock Frequency"
msgstr "Frequência Média do Relógio"

#: SensorManager.cpp:162
#, kde-format
msgid "Hardware Sensors"
msgstr "Sensores por 'Hardware'"

#: SensorManager.cpp:163
#, kde-format
msgid "Partition Usage"
msgstr "Utilização da Partição"

#: SensorManager.cpp:164
#, kde-format
msgid "Used Space"
msgstr "Espaço Usada"

#: SensorManager.cpp:165
#, kde-format
msgid "Free Space"
msgstr "Espaço Livre"

#: SensorManager.cpp:166
#, kde-format
msgid "Fill Level"
msgstr "Nível de Preenchimento"

#: SensorManager.cpp:167
#, kde-format
msgid "Used Inodes"
msgstr "I-nodes Usados"

#: SensorManager.cpp:168
#, kde-format
msgid "Free Inodes"
msgstr "I-nodes Livres"

#: SensorManager.cpp:169
#, kde-format
msgid "Inode Level"
msgstr "Nível do I-Node"

#: SensorManager.cpp:170
#, kde-format
msgid "System"
msgstr "Sistema"

#: SensorManager.cpp:171
#, kde-format
msgid "Uptime"
msgstr "Tempo de Actividade"

#: SensorManager.cpp:172
#, kde-format
msgid "Linux Soft Raid (md)"
msgstr "RAID de Linux por 'Software' (md)"

#: SensorManager.cpp:173
#, kde-format
msgid "Processors"
msgstr "Processadores"

#: SensorManager.cpp:174
#, kde-format
msgid "Cores"
msgstr "Núcleos"

#: SensorManager.cpp:175
#, kde-format
msgid "Number of Blocks"
msgstr "Número de Blocos"

#: SensorManager.cpp:176
#, kde-format
msgid "Total Number of Devices"
msgstr "Número Total de Dispositivos"

#: SensorManager.cpp:177
#, kde-format
msgid "Failed Devices"
msgstr "Dispositivos em Falha"

#: SensorManager.cpp:178
#, kde-format
msgid "Spare Devices"
msgstr "Dispositivos Disponíveis"

#: SensorManager.cpp:179
#, kde-format
msgid "Number of Raid Devices"
msgstr "Número de Dispositivos RAID"

#: SensorManager.cpp:180
#, kde-format
msgid "Working Devices"
msgstr "Dispositivos Funcionais"

#: SensorManager.cpp:181
#, kde-format
msgid "Active Devices"
msgstr "Dispositivos Activos"

#: SensorManager.cpp:182
#, kde-format
msgid "Number of Devices"
msgstr "Número de Dispositivos"

#: SensorManager.cpp:183
#, kde-format
msgid "Resyncing Percent"
msgstr "Percentagem da Sincronização"

#: SensorManager.cpp:184
#, kde-format
msgid "Disk Information"
msgstr "Informação do Disco"

#: SensorManager.cpp:185
#, kde-format
msgid "CPU Temperature"
msgstr "Temperatura do CPU"

#: SensorManager.cpp:186
#, kde-format
msgid "Motherboard Temperature"
msgstr "Temperatura da 'Motherboard'"

#: SensorManager.cpp:187
#, kde-format
msgid "Power Supply Temperature"
msgstr "Temperatura da Fonte de Alimentação"

#: SensorManager.cpp:189
#, kde-format
msgid "Filesystem Root"
msgstr "Raiz do Sistema de Ficheiros"

#: SensorManager.cpp:192
#, kde-format
msgid "Extra Temperature Sensor %1"
msgstr "Sensor de Temperatura Extra %1"

#: SensorManager.cpp:196
#, kde-format
msgid "PECI Temperature Sensor %1"
msgstr "Sensor de Temperatura PECI %1"

#: SensorManager.cpp:197
#, kde-format
msgid "PECI Temperature Calibration %1"
msgstr "Calibração da Temperatura PECI %1"

#: SensorManager.cpp:201
#, kde-format
msgid "CPU %1"
msgstr "CPU %1"

#: SensorManager.cpp:202
#, kde-format
msgid "Disk %1"
msgstr "Disco %1"

#: SensorManager.cpp:206
#, kde-format
msgid "Battery %1"
msgstr "Bateria %1"

#: SensorManager.cpp:207
#, kde-format
msgid "Fan %1"
msgstr "Ventoinha %1"

#: SensorManager.cpp:208
#, kde-format
msgid "Temperature %1"
msgstr "Temperatura %1"

#: SensorManager.cpp:211
#, kde-format
msgid "Total"
msgstr "Total"

#: SensorManager.cpp:212
#, kde-format
msgid "Software Interrupts"
msgstr "Interrupções por 'Software'"

#: SensorManager.cpp:213
#, kde-format
msgid "Hardware Interrupts"
msgstr "Interrupções por 'Hardware'"

#: SensorManager.cpp:218 SensorManager.cpp:220
#, kde-format
msgid "Int %1"
msgstr "Int %1"

#: SensorManager.cpp:223
#, kde-format
msgid "Link Quality"
msgstr "Qualidade da Ligação"

#: SensorManager.cpp:224
#, kde-format
msgid "Signal Level"
msgstr "Nível de Sinal"

#: SensorManager.cpp:225
#, kde-format
msgid "Noise Level"
msgstr "Nível de Ruído"

#: SensorManager.cpp:226
#, kde-format
msgid "Rx Invalid Nwid Packets"
msgstr "Pacotes Rx com Nwid Inválido"

#: SensorManager.cpp:227
#, kde-format
msgid "Total Rx Invalid Nwid Packets"
msgstr "Total de Pacotes Rx com Nwid Inválido"

#: SensorManager.cpp:228
#, kde-format
msgid "Rx Invalid Crypt Packets"
msgstr "Pacotes Rx com Cifra Inválida"

#: SensorManager.cpp:229
#, kde-format
msgid "Total Rx Invalid Crypt Packets"
msgstr "Total de Pacotes Rx com Cifra Inválida"

#: SensorManager.cpp:230
#, kde-format
msgid "Rx Invalid Frag Packets"
msgstr "Pacotes Rx com Fragmentação Inválida"

#: SensorManager.cpp:231
#, kde-format
msgid "Total Rx Invalid Frag Packets"
msgstr "Total de Pacotes Rx com Fragmentação Inválida"

#: SensorManager.cpp:232
#, kde-format
msgid "Tx Excessive Retries Packets"
msgstr "Pacotes Tx com Excesso de Repetições"

#: SensorManager.cpp:233
#, kde-format
msgid "Total Tx Excessive Retries Packets"
msgstr "Total de Pacotes Tx com Excesso de Repetições"

#: SensorManager.cpp:234
#, kde-format
msgid "Invalid Misc Packets"
msgstr "Pacotes Diversos Inválidos"

#: SensorManager.cpp:235
#, kde-format
msgid "Total Invalid Misc Packets"
msgstr "Total de Pacotes Diversos Inválidos"

#: SensorManager.cpp:236
#, kde-format
msgid "Missed Beacons"
msgstr "Emissões de Sinal Perdidas"

#: SensorManager.cpp:237
#, kde-format
msgid "Total Missed Beacons"
msgstr "Total de Emissões de Sinal Perdidas"

#: SensorManager.cpp:239
#, kde-format
msgid "Log Files"
msgstr "Ficheiros de Registo"

#: SensorManager.cpp:243
#, kde-format
msgctxt "the unit 1 per second"
msgid "1/s"
msgstr "1/s"

#: SensorManager.cpp:244
#, kde-format
msgid "kBytes"
msgstr "kBytes"

#: SensorManager.cpp:245
#, kde-format
msgctxt "the unit minutes"
msgid "min"
msgstr "min"

#: SensorManager.cpp:246
#, kde-format
msgctxt "the frequency unit"
msgid "MHz"
msgstr "MHz"

#: SensorManager.cpp:247
#, kde-format
msgctxt "a percentage"
msgid "%"
msgstr "%"

#: SensorManager.cpp:248
#, kde-format
msgctxt "the unit milliamperes"
msgid "mA"
msgstr "mA"

#: SensorManager.cpp:249
#, kde-format
msgctxt "the unit milliampere hours"
msgid "mAh"
msgstr "mAh"

#: SensorManager.cpp:250
#, kde-format
msgctxt "the unit milliwatts"
msgid "mW"
msgstr "mW"

#: SensorManager.cpp:251
#, kde-format
msgctxt "the unit milliwatt hours"
msgid "mWh"
msgstr "mWh"

#: SensorManager.cpp:252
#, kde-format
msgctxt "the unit millivolts"
msgid "mV"
msgstr "mV"

#: SensorManager.cpp:255
#, kde-format
msgid "Integer Value"
msgstr "Valor Inteiro"

#: SensorManager.cpp:256
#, kde-format
msgid "Floating Point Value"
msgstr "Valor de Vírgula-Flutuante"

#: SensorManager.cpp:259
#, kde-format
msgid "Log File"
msgstr "Ficheiro de Registo"

#: SensorShellAgent.cpp:108
#, kde-format
msgid "Could not run daemon program '%1'."
msgstr "Não foi possível executar o programa '%1'."

#: SensorShellAgent.cpp:115
#, kde-format
msgid "The daemon program '%1' failed."
msgstr "O programa-servidor '%1' terminou sem sucesso."

#: SensorSocketAgent.cpp:89
#, kde-format
msgid "Connection to %1 refused"
msgstr "Foi recusada a ligação a %1"

#: SensorSocketAgent.cpp:92
#, kde-format
msgid "Host %1 not found"
msgstr "A máquina %1 não foi encontrada"

#: SensorSocketAgent.cpp:95
#, kde-format
msgid ""
"An error occurred with the network (e.g. the network cable was accidentally "
"unplugged) for host %1."
msgstr ""
"Ocorreu um erro na rede (p.ex., o cabo de rede foi desligado sem querer) "
"para a máquina %1."

#: SensorSocketAgent.cpp:98
#, kde-format
msgid "Error for host %1: %2"
msgstr "Erro da máquina %1: %2"
