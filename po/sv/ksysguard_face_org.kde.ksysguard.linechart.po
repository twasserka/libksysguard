# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-07 00:17+0000\n"
"PO-Revision-Date: 2021-01-06 12:18+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: contents/ui/Config.qml:42
#, kde-format
msgid "Appearance"
msgstr "Utseende"

#: contents/ui/Config.qml:47
#, kde-format
msgid "Show Sensors Legend"
msgstr "Visa sensorbeskrivning"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Stacked Charts"
msgstr "Staplade diagram"

#: contents/ui/Config.qml:55
#, kde-format
msgid "Smooth Lines"
msgstr "Jämna linjer"

#: contents/ui/Config.qml:59
#, kde-format
msgid "Show Grid Lines"
msgstr "Visa rutnät"

#: contents/ui/Config.qml:63
#, kde-format
msgid "Show Y Axis Labels"
msgstr "Visa Y-axelbeteckningar"

#: contents/ui/Config.qml:67
#, kde-format
msgid "Fill Opacity:"
msgstr "Ifyllnadsogenomskinlighet:"

#: contents/ui/Config.qml:73
#, kde-format
msgid "Data Ranges"
msgstr "Dataintervall"

#: contents/ui/Config.qml:78
#, kde-format
msgid "Automatic Y Data Range"
msgstr "Automatiskt Y-dataintervall"

#: contents/ui/Config.qml:82
#, kde-format
msgid "From (Y):"
msgstr "Från (Y):"

#: contents/ui/Config.qml:89
#, kde-format
msgid "To (Y):"
msgstr "Till (Y):"

#: contents/ui/Config.qml:99
#, kde-format
msgid "Amount of History to Keep:"
msgstr "Mängden historik som behålls:"

#: contents/ui/Config.qml:102
#, kde-format
msgctxt "%1 is seconds of history"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekund"
msgstr[1] "%1 sekunder"

#~ msgid "Automatic X Data Range"
#~ msgstr "Automatiskt X-dataintervall"

#~ msgid "From (X):"
#~ msgstr "Från (X):"

#~ msgid "To (X):"
#~ msgstr "Till (X):"
