# Chinese translations for libksysguard package
# libksysguard 套件的正體中文翻譯.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the libksysguard package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-07 00:17+0000\n"
"PO-Revision-Date: 2021-08-07 00:17+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: contents/ui/Config.qml:35
#, kde-format
msgid "Show Sensors Legend"
msgstr ""

#: contents/ui/Config.qml:39
#, kde-format
msgid "Stacked Bars"
msgstr ""

#: contents/ui/Config.qml:43
#, kde-format
msgid "Show Grid Lines"
msgstr ""

#: contents/ui/Config.qml:47
#, kde-format
msgid "Show Y Axis Labels"
msgstr ""

#: contents/ui/Config.qml:51
#, kde-format
msgid "Automatic Data Range"
msgstr ""

#: contents/ui/Config.qml:55
#, kde-format
msgid "From:"
msgstr ""

#: contents/ui/Config.qml:62
#, kde-format
msgid "To:"
msgstr ""
