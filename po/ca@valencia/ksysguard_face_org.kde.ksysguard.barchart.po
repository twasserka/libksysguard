# Translation of ksysguard_face_org.kde.ksysguard.barchart.po to Catalan (Valencian)
# Copyright (C) 2020 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
# or the same license as the source of its messages in English.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-07 00:17+0000\n"
"PO-Revision-Date: 2020-07-25 11:30+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: contents/ui/Config.qml:35
#, kde-format
msgid "Show Sensors Legend"
msgstr "Mostra la llegenda dels sensors"

#: contents/ui/Config.qml:39
#, kde-format
msgid "Stacked Bars"
msgstr "Barres apilades"

#: contents/ui/Config.qml:43
#, kde-format
msgid "Show Grid Lines"
msgstr "Mostra les línies de la quadrícula"

#: contents/ui/Config.qml:47
#, kde-format
msgid "Show Y Axis Labels"
msgstr "Mostra les etiquetes de l'eix Y"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Automatic Data Range"
msgstr "Interval automàtic de les dades"

#: contents/ui/Config.qml:55
#, kde-format
msgid "From:"
msgstr "Des de:"

#: contents/ui/Config.qml:62
#, kde-format
msgid "To:"
msgstr "Fins a:"
